package de.uni.heidelberg.CrashFinder;

/**
 *
 * @author Antsa Harinala Andriamboavonjy, Dominik Fay
 * Created in October 2015.
 */
public interface CrashFinderRunner {
    
    public void runner();
           
    
}
